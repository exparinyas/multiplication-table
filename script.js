const inputNumber = document.getElementById('input-number');
const btnOutput = document.getElementById('btn-output');
const showMe = document.getElementById('show-me');
const clear = document.getElementById('clear');

function showMultiplication() {
    const number = inputNumber.value;
    const integerNumber = Math.floor(number); // ใช้ปัดเศษทศนิยมให้เป็นเลขจำนวนเต็ม และให้มีค่าน้อยลง
    let outputShowme = '';

    if (integerNumber < 2) {
        showMe.innerHTML = 'โปรดใส่ตัวเลข 2 ถึง 25';
        showMe.style.padding = '10px';
        return;
    }
    else if (integerNumber > 25) {
        showMe.innerHTML = 'สูตรคูณมีถึงแม่ 25 เท่านั้น!';
        showMe.style.padding = '10px';
        return;
    }
    
    for (let i = 1; i <= 12; i++) {
        // += คือ บวกเพิ่มเข้ากับตัวแปรที่กำหนดเป็นแถวยาวเหมือนรถไฟ ข้อความก็จะแสดงเป็นข้อความ ตัวแปรก็จะคำนวณออกมา ต่อกันตามลำดับ
        // outputShowme += '<p>';
        // outputShowme += '<p>' + integerNumber + ' x ' + i + ' = ' + (integerNumber * i) + '</p>';
        // outputShowme += '</p>';
        outputShowme += `<p>${integerNumber} x ${i} = ${(integerNumber * i)}</p>`
    }
    
    showMe.style.padding = '10px';
    showMe.innerHTML = outputShowme;
}

function clearMultiplication() {
    const clearInput = inputNumber;
    const clearMulti = showMe;

    // กำหนดให้เป็นค่าว่าง
    clearInput.value = null;
    clearMulti.innerHTML = null;
    clearMulti.style.padding = null;
}

btnOutput.addEventListener('click', showMultiplication);
clear.addEventListener('click', clearMultiplication);